package main

import (
	"fmt"
	"net/http"
	"log"
	"html"
	p "go_rest/src/endpoint"
)

func main() {
	fmt.Println("Serwer running in http://localhost:8080")
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
	})

	http.HandleFunc("/hello", p.HelloWorld)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
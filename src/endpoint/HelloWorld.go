package endpoint

import (
	"net/http"
	"encoding/json"
	"log"
)

func HelloWorld(w http.ResponseWriter, r *http.Request){
	SetCors(w,r)
	log.Println("/hello")
	json.NewEncoder(w).Encode("Hello World, Golang Rest Api")
}
